import 'dart:math';

import 'package:flutter/widgets.dart';

// Useful methods
extension ContextExtensions on BuildContext {
  double w() => MediaQuery.of(this).size.width;
  double h() => MediaQuery.of(this).size.height;
  double minDimension() => min(w(), h());
  SizedBox hBox(double percentage) => SizedBox(height: h() * percentage);
  SizedBox wBox(double percentage) => SizedBox(width: w() * percentage);
}
