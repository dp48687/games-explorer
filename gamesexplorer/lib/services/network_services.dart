import 'package:dio/dio.dart';
import 'package:gamesexplorer/models.dart/game_model.dart';
import 'package:gamesexplorer/models.dart/genre_model.dart';

class Downloader {
  static final Dio _dio = Dio();
  static const String _key = "320eebae9994428db328e0db80905eba";

  static Future<List<GameModel>> downloadGameData(String genre) async {
    final response = await _dio.get(
      "https://api.rawg.io/api/games?key=$_key&genres=${genre.toLowerCase()}",
    );
    List<GameModel> games = [];
    for (Map<String, dynamic> model in response.data["results"]) {
      games.add(GameModel.fromMap(model));
    }
    return games;
  }

  static Future<List<GenreModel>> downloadGenres() async {
    final response = await _dio.get("https://api.rawg.io/api/genres?key=$_key");
    List<GenreModel> genres = [];
    for (Map<String, dynamic> model in response.data["results"]) {
      genres.add(GenreModel.fromMap(model));
    }
    return genres;
  }
}
