import 'package:flutter/cupertino.dart';
import 'package:gamesexplorer/models.dart/game_model.dart';
import 'package:gamesexplorer/models.dart/genre_model.dart';
import 'package:gamesexplorer/services/network_services.dart';

class GameCollectionModel extends ChangeNotifier {
  List<GameModel> games = [];

  Future<GameCollectionModel> downloadGames(GenreModel genre) async {
    games = await Downloader.downloadGameData(genre.name);
    notifyListeners();
    return this;
  }
}
