import 'dart:convert';
import 'package:flutter/cupertino.dart';

class GameModel extends ChangeNotifier {
  int id;
  String name;
  String released;
  String backgroundImageUrl;
  double rating;
  double ratingTop;
  int ratingsCount;
  List<String> genres;
  List<String> platforms;
  List<String> stores;

  GameModel({
    required this.id,
    required this.name,
    required this.released,
    required this.backgroundImageUrl,
    required this.rating,
    required this.ratingTop,
    required this.ratingsCount,
    required this.genres,
    required this.platforms,
    required this.stores,
  });

  factory GameModel.fromMap(Map<String, dynamic> map) {
    List<String> platforms = [];
    for (var platform in map["platforms"]) {
      platforms.add(platform["platform"]["name"]);
    }
    List<String> stores = [];
    for (var store in map["stores"]) {
      stores.add(store["store"]["name"]);
    }
    List<String> genres = [];
    for (var genre in map["genres"]) {
      genres.add(genre["name"]);
    }
    return GameModel(
      id: map['id'],
      name: map['name'],
      released: map['released'],
      backgroundImageUrl: map["background_image"],
      rating: map['rating'],
      ratingTop: map['rating_top'],
      ratingsCount: map['ratings_count'],
      genres: genres,
      platforms: platforms,
      stores: stores,
    );
  }

  factory GameModel.fromJson(String source) =>
      GameModel.fromMap(json.decode(source));
}
