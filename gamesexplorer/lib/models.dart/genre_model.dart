import 'dart:convert';

class GenreModel {
  String name;
  String backgroundImage;
  int games;

  GenreModel({
    required this.name,
    required this.backgroundImage,
    required this.games,
  });

  factory GenreModel.fromMap(Map<String, dynamic> map) => GenreModel(
        name: map['name'],
        backgroundImage: map['image_background'],
        games: map['games_count'],
      );

  factory GenreModel.fromJson(String source) =>
      GenreModel.fromMap(json.decode(source));
}
