import 'package:flutter/material.dart';
import 'package:gamesexplorer/pages/genre_list_page.dart';
import 'package:gamesexplorer/providers/game_provider.dart';
import 'package:gamesexplorer/providers/genre_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MaterialApp(
      home: MultiProvider(
        providers: [
          genreProvider,
          gameListProvider,
        ],
        child: const GenreListPageWidget(),
      ),
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueGrey,
      ),
      debugShowCheckedModeBanner: false,
    ),
  );
}
