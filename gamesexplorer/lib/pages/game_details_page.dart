import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gamesexplorer/models.dart/game_model.dart';
import 'package:gamesexplorer/services/screen_services.dart';
import 'package:provider/provider.dart';

TextStyle buttonTextStyle() => const TextStyle(
      fontSize: 25,
      color: Colors.white,
      fontWeight: FontWeight.bold,
      decoration: TextDecoration.none,
    );

class GameDetailsPageWidget extends StatelessWidget {
  const GameDetailsPageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Consumer<GameModel>(
      builder: (contextOut, gameModel, _) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                color: Colors.blue[200],
                height: context.h() * 0.2,
                width: context.w(),
                alignment: Alignment.center,
                child: Text(
                  gameModel.name,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
              context.hBox(0.05),
              Row(
                children: [
                  SizedBox(
                    width: context.minDimension() * 0.05,
                  ),
                  Image.network(
                    gameModel.backgroundImageUrl,
                    width: context.w() * 0.283333,
                    height: context.h() * 0.35,
                  ),
                  context.wBox(0.05),
                  Container(
                    height: context.h() * 0.35,
                    width: context.w() * 0.6167,
                    color: Colors.grey[100],
                    child: Padding(
                      padding: EdgeInsets.all(context.minDimension() * 0.025),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: AutoSizeText(
                              "Released: ${gameModel.released}",
                              maxLines: 1,
                              style: const TextStyle(
                                fontSize: 50,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              "Rating (${gameModel.ratingsCount} votes):",
                              style: const TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.none,
                              ),
                            ),
                          ),
                          Expanded(
                            child: RatingBarIndicator(
                              rating:
                                  gameModel.rating / gameModel.ratingTop * 5,
                              itemBuilder: (context, index) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              itemCount: 5,
                              itemSize: 50.0,
                              direction: Axis.horizontal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              context.hBox(0.02),
              Container(
                width: context.w(),
                height: context.h() * 0.2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(context.minDimension() * 0.05),
                        child: ElevatedButton(
                            onPressed: () {},
                            child: AutoSizeText(
                              "Show genres",
                              maxLines: 1,
                              style: buttonTextStyle(),
                            )),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(context.minDimension() * 0.05),
                        child: ElevatedButton(
                            onPressed: () {},
                            child: AutoSizeText(
                              "Show platforms",
                              maxLines: 1,
                              style: buttonTextStyle(),
                            )),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(context.minDimension() * 0.05),
                        child: ElevatedButton(
                            onPressed: () {},
                            child: AutoSizeText(
                              "Show stores",
                              maxLines: 1,
                              style: buttonTextStyle(),
                            )),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ));
}
