import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gamesexplorer/models.dart/game_collection_model.dart';
import 'package:gamesexplorer/models.dart/game_model.dart';
import 'package:gamesexplorer/pages/game_details_page.dart';
import 'package:gamesexplorer/services/screen_services.dart';
import 'package:provider/provider.dart';

class GameListPageWidget extends StatelessWidget {
  const GameListPageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Consumer<GameCollectionModel>(
        builder: (ctx, listOfGames, _) => ListView.builder(
          itemCount: listOfGames.games.length,
          itemBuilder: (ctx, index) => GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                    builder: (_, __) => const GameDetailsPageWidget(),
                    create: (_) => listOfGames.games[index],
                  ),
                ),
              );
            },
            child: Container(
              width: context.w(),
              height: context.h() * 0.22,
              color: index % 2 == 0 ? Colors.white : Colors.blue[50],
              child: Flex(
                direction: Axis.horizontal,
                children: [
                  Expanded(
                    child: Text(
                      listOfGames.games[index].name,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
