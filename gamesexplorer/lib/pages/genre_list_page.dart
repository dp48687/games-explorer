import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gamesexplorer/models.dart/game_collection_model.dart';
import 'package:gamesexplorer/models.dart/genre_model.dart';
import 'package:gamesexplorer/services/screen_services.dart';
import 'package:provider/provider.dart';

import 'game_list_page.dart';

class GenreListPageWidget extends StatelessWidget {
  const GenreListPageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext contextOutside) {
    return Consumer<List<GenreModel>>(
      builder: (contextConsumer, listOfGenres, _) => ListView.builder(
        itemCount: listOfGenres.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () async {
            var games =
                await GameCollectionModel().downloadGames(listOfGenres[index]);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ChangeNotifierProvider<GameCollectionModel>(
                  create: (_) => games,
                  builder: (_, __) => const GameListPageWidget(),
                ),
              ),
            );
          },
          child: Container(
            width: context.w(),
            height: context.h() * 0.22,
            color: index % 2 == 0 ? Colors.white : Colors.blue[50],
            child: Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  child: Text(
                    listOfGenres[index].name,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
