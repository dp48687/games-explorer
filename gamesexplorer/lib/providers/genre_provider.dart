import 'package:gamesexplorer/models.dart/genre_model.dart';
import 'package:gamesexplorer/services/network_services.dart';
import 'package:provider/provider.dart';

FutureProvider<List<GenreModel>> genreProvider =
    FutureProvider<List<GenreModel>>(
        catchError: (ctx, errorObject) {
          return emptyListOfGenres();
        },
        create: (context) => Downloader.downloadGenres(),
        initialData: emptyListOfGenres());

List<GenreModel> emptyListOfGenres() {
  List<GenreModel> models = [
    GenreModel(name: "", backgroundImage: "-", games: 1)
  ];
  models.removeAt(0);
  return models;
}
