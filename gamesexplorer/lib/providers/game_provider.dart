import 'package:gamesexplorer/models.dart/game_collection_model.dart';
import 'package:gamesexplorer/models.dart/genre_model.dart';
import 'package:provider/provider.dart';

ProxyProvider<GenreModel, Future<GameCollectionModel>> gameListProvider =
    ProxyProvider(
  create: (ctx) async => GameCollectionModel(),
  update: (ctx, genreModel, games) async =>
      ((await games!).downloadGames(genreModel)),
);
